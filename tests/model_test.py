import model


def test_roll_dice_with_1():
    results = model.roll_dice(1)
    assert len(results) == 1
    assert 7 > results[0] > 0


def test_roll_dice_with_3():
    results = model.roll_dice(3)
    assert len(results) == 3
    for result in results:
        assert 7 > result > 0
