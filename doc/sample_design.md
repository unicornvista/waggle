# Work Flow for Waggle

[toc]

## SUB `main_loop`

```
LOOP
    valid_input = get_valid_input()
    IF(valid_input.quit)
        cleanup()
        EXIT
    roll_results = roll_dice(valid_input.dice_count)
    display(roll_results)
```

### FUNC `get_valid_input`

```
LOOP
    PRINT("Please enter the number of dice to roll, or 'q' to quit:")
    answer = INPUT
    IF(answer IS NUMERIC or Q or q)
        RETURN = answer
    ELSE
        PRINT("Invalid answer type.")
```

### FUNC `roll_dice`

```
INPUT: dice_count

result = ARRAY
FOR COUNT dice_count
    result.ADD(roll_die())
RETURN = result
```

### SUB `display_results`

```
INPUT: roll_results

FOR EACH roll_results AS roll
    image = transform(roll)
    PRINT(image)
```

#### FUNC `roll_die`

```
RETURN = RANDOM BETWEEN 1, 6
```

#### FUNC `transform`

```
INPUT: roll

image = GENERATE ASCII ART FOR roll
RETURN = image
```

