import model
import waggleio as io


def main():
    io.print_introduction()

    running = True
    while running:
        answer = io.get_valid_input()
        if not answer:
            running = False
        else:
            results = model.roll_dice(answer)
            io.display_results(results)


if __name__ == '__main__':
    main()
