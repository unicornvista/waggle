def print_introduction():
    print("Welcome to Waggle, the legendary dice rolling tool.")
    # TODO: make a fancy introduction


def get_valid_input():
    while True:
        answer = input("How many dice are you rolling? (Q to quit): ")
        if not answer:
            print("Invalid input")
            continue
        if answer.lower() == 'q':
            return False
        if not answer.isdigit():
            print("Must enter a whole number.")
            continue
        # TODO: enforce numeric range of dice rolls:w

        return int(answer)


def display_results(results):
    print("Your results: ", end="")
    for result in results:
        print(f"|{result}| ", end="")
    print()
    # TODO: make the dice look more dicey
